Role Name
=========

An Ansible role that installs docker on Ubuntu 20.04.

Provides configuration options to set up insecure registries, registries
with custom certificates, users that have access to it and the cgroup 
driver.

Requirements
------------

None.

Role Variables
--------------

### Insecure registries ###

The role allows to define a list of insecure docker registries. When
docker tries to pull or push images to it, any error derived from a
malformed (or lack of) ssl certificate will be ignored by docker.
This is highly insecure and vulnerable to MITM attacks.

To set up insecure registries, define the variable 
`docker_insecure_registries` with a list. Every element of the list
should be a string formed by the fqdn or ip of the insecure registry
host followed by `:` and by the port of the insecure registry.
For example:

```yml
docker_insecure_registries:
 - "some-insecure-registry.local:5050"
 - "192.168.1.2:5000"
```

Defaults to an empty list.


### Registries with custom certificates ###

Another way to access custom registries without a public certificate
signed by a trusted authority is to use a self-signed or custom-ca
signed certificate in the registry. To access this registry, docker
can still use SSL to encrypt and secure the communication. The only
thing docker needs is a way to recognize the certificate. This is 
achieved copying the public certificate into
`/etc/docker/certs.d/<registry-fqdn>:<registry-port>/ca.crt`.

This role has support to copy certificates from other machines 
accesibles to ansible to this server. The way it works is specifically
designed to work well with the 
['ca_based_certificate`](https://gitlab.com/galvesband-ansible/ca_based_certificate)
role, but should do its job as long as ansible is able to access
the machine holding the certificate.

Define the variable `docker_trusted_registries_certs` with a list
of dicts like this:

```yml
docker_trusted_registries_certs:
    # DNS record of registry.
  - fqdn: name.of.registry.local
    # Port where the registry is accesible.
    port: 5050
    # Path where the certificate is located 
    # on the remote host that owns the certificate.
    cert_src: /srv/certs/name.of.registry.local/cert.crt
    # Inventory name of the server where the certificate is located.
    cert_host: some-other-server
  - fqdn: [...]
```

Defaults to an empty list.


### Users with access to docker ###

You can specify a list of users in the server that should
be allowed access to docker. The role will grant permissions
(adding those users to the `docker` group in the server).

```yml
docker_users:
  - peter
  - sally
```

Defaults to an empty list.


### Docker cgroup driver ###

Specify the cgroup driver docker should use. This is particularly
usefull setting up kubernetes, with refuses to start if docker
use any other cgroup driver other than `systemd`, which is not
the default in docker.

```yml
docker_cgroup_driver: systemd
```

Defaults to `systemd`.


Dependencies
------------

None.

Example Playbook
----------------

Very simple standard example:

```yml
- name: Docker
  hosts: some-host
  roles:
    - name: docker
      vars:
        docker_users:
          - myself
```

A more convoluted example with many variables:

```yml
- name: Spicy docker
  hosts: some-other-host-or-group
  roles:
    - name: docker
      vars:

        # List of users with access to docker
        docker_users:
          - myself
          - myotherself

        # List of insecure registries
        docker_insecure_registries:
          - "localhost:5000"
          - "192.16.1.2:5000"
          - "insecure.example.org:5000"

        # List of trusted registries with a certificate
        docker_trusted_registries_certs:
          - fqdn: my-secured-registry.local
            port: 5050
            cert_src: /srv/certs/my-secured-registry.local/cert.crt
            cert_host: host-owner-of-cert

        # Install docker-compose too
        docker_install_compose: yes
```


License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
